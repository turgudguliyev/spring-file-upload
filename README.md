## Run
1. Execute this command on terminal
   ```bash
   docker compose up -d
   ```
2. Run application on IntelliJ IDEA

## Swagger

[Swagger Link](http://localhost:8080/swagger-ui/index.html)

## Database

If you want see database tables:

```bash
docker exec -it db /bin/sh
```
```
PGPASSWORD=password psql -U postgres -d postgres
```
```
SELECT * FROM files;
```