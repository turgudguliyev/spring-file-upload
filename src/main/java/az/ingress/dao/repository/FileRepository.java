package az.ingress.dao.repository;

import az.ingress.dao.entity.FileEntity;
import org.springframework.data.repository.CrudRepository;

public interface FileRepository extends CrudRepository<FileEntity, Long> { }