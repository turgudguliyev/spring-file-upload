package az.ingress.service;

import az.ingress.dao.entity.FileEntity;
import az.ingress.dao.repository.FileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;

import static java.util.Base64.getDecoder;
import static java.util.Base64.getEncoder;
import static java.util.UUID.randomUUID;

@Service
@RequiredArgsConstructor
public class FileService {
    private final FileRepository fileRepository;

    public void save(MultipartFile file) {
        try {
            var entity = FileEntity.builder()
                    .name(file.getOriginalFilename())
                    .content(getEncoder().encodeToString(file.getBytes()))
                    .build();
            fileRepository.save(entity);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public Resource getById(Long id) {
        var entity = fetchIfExist(id);
        var bytes = getDecoder().decode(entity.getContent());
        return new ByteArrayResource(bytes);
    }

    private FileEntity fetchIfExist(Long id) {
        return fileRepository.findById(id)
                .orElseThrow();
    }
}